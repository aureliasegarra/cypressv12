/// <reference types="cypress" />

Cypress.Commands.addQuery('text', () => {
  return ($elements) => Cypress._.map($elements, 'innerText')
})

  const getFruits = ($el) => {
    return Cypress._.map($el, 'innerText')
  }

  it.skip('confirms the list without retry', () => {
    cy.visit('cypress/index.html');
    cy.get('.matching')
        .then(getFruits)
        .should('deep.equal', ['Banane', 'Poire', 'Mangue']);
  })

  it('confirms the list', () => {
    cy.visit('cypress/index.html');
    cy.get('.matching')
        .text('innerText')
        .should('deep.equal', ['Banane', 'Poire', 'Mangue']);
  })
