/// <reference types="cypress" />

Cypress.Commands.addQuery('map', (fnOrProp) => {
    return ($el) => Cypress._.map($el, fnOrProp);
})
